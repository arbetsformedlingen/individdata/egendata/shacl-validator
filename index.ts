import fs from 'fs';
import SHACLValidator from 'rdf-validate-shacl';
import N3 from 'n3';
import factory from 'rdf-ext';

const parseDataset = (filepath: string) => {
  const data = fs.readFileSync(filepath, { encoding:'utf8', flag:'r' });
  const quads = new N3.Parser().parse(data);
  return factory.dataset(quads);
}

const isValidShapeInstance = (shapeFilepath: string, instanceFilepath: string) => {
  const shape = parseDataset(shapeFilepath);
  const instance = parseDataset(instanceFilepath);
  const validator = new SHACLValidator(shape);
  const report = validator.validate(instance);

  if (!report.conforms) {
    for (const result of report.results) {
      // See https://www.w3.org/TR/shacl/#results-validation-result for details
      const {
        message,
        path,
        focusNode,
        severity,
        sourceConstraintComponent,
        sourceShape,
      } = result;
      console.log('\n=== VALIDATION RESULT BEGIN ===');
      console.log('[message]', message);
      console.log('[path]', path);
      console.log('[focusNode]', focusNode);
      console.log('[severity]', severity);
      console.log('[sourceConstraintComponent]', sourceConstraintComponent);
      console.log('[sourceShape]', sourceShape);
      console.log('=== VALIDATION RESULT END ===\n');
    }
  }
  return report.conforms;
}

const isValid = isValidShapeInstance('./data/test.shacl', './data/test.ttl');

if (isValid) {
  console.log('The instance is VALID');
} else {
  console.log('The instance is INVALID');
}
